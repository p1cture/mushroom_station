#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <SD.h>
#include "DHTesp.h"
#include "SparkFun_SGP30_Arduino_Library.h"
#include <ESP8266WiFiMulti.h>

#define sdChipSelect D0
#define switchPin A0

#define fileName "log.txt"
#define credsFileName "wifi.txt"

#define dataLen 100 // amount of data Points that get shown on the screen
#define dataAmount 2
#define graphWidth 100
#define graphHeight 50

#define AP_SSID "mushroooooms"
#define AP_PASS "clearly_edible"

//128x64
Adafruit_SSD1306 display(0);

SGP30 sgp; //air quality sensor
DHTesp dht;//humitidy and temp sensor
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org",3600,60000); //internet clock
ESP8266WiFiMulti wifiMulti;
const uint32_t connectTimeoutMs = 5000;

File logFile;

typedef struct {
  float data[dataLen];
  uint8_t head;
}Fifo;

Fifo data[dataAmount] = {};

const int limits[dataAmount] = {100, 50/*, 4000, 1000*/};
const char header[dataAmount][4] = {
  "HMD",
  "TMP",/*
  "CO2",
  "VOC"*/
};
float new_data[dataAmount];
unsigned long startEpoch;
uint8_t current_menu = 0;

bool hasSD = false;
bool hasSGP = false;
unsigned long t1, t2, t3, epoch;

int switchState = true;             // the current reading from the input pin
int lastSwitchState = LOW;   // the previous reading from the input pin
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

TempAndHumidity dhtData;

//This gets called when the arduino powers on (or resets)
void setup() {
  Wire.begin(); //sda,scl
 // Wire.setClock(400000);
  Serial.begin(115200);
  while (!Serial) {}
  Serial.println("Init");
  Serial.println();


  //Init data
  for(int i=0;i<dataAmount;i++) {
    data[i] = {{},0};
  }

  //Init display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.clearDisplay();
  display.display();
  display.setTextSize(1);
  display.setTextColor(WHITE);

  //init SD Cards
  pinMode(D8, OUTPUT);
  if (SD.begin(D8)) {
    hasSD = true;
    Serial.println("SD card is present & ready");
  } else {
    hasSD = false;
    Serial.println("SD card missing or failure");
  }

  //Init Switch
  pinMode(switchPin, INPUT_PULLUP);
  switchState = analogRead(switchPin) < 500 ? true: false;

  //Init DHT
  dht.setup(3, DHTesp::DHT11);
  
  connectWifi();
 
  Serial.println();

  //Init Internet Clock
  timeClient.update();
  startEpoch = timeClient.getEpochTime();

  //Init SGP
  hasSGP = sgp.begin();
  if (hasSGP == false) {
    Serial.println("No SGP30 Detected. Check connections.");
  } else {
      sgp.initAirQuality();
      delay(1000);
      sgp.measureAirQuality();
  }

  //Init timers
  t1 = millis();
  t2 = millis();
  t3 = millis();
  Serial.println("Init End");
}

// This is what get called while the arduino is running as fast as it can
void loop() {
  
  //Update DHT
  if (millis() >= t2 + dht.getMinimumSamplingPeriod() + 2000) {// Every 2 sec
    t2 = millis();

    dhtData = dht.getTempAndHumidity();
    new_data[0] = dhtData.humidity;
    new_data[1] = dhtData.temperature;
  
    if (hasSGP && new_data[0] != 0 && new_data[1] != 0) {
   //     sgp.setHumidity(getAbsoluteHumidity(new_data[1], new_data[0])); 
    }
  }

  // Update Display and SGP
  if (millis() >= t1 + 1000) {// Every 1 sec
    t1 = millis();

    //get data of air Sensor
    sgp.measureAirQuality();
    if (hasSGP) {
      new_data[2] = sgp.CO2;
      new_data[3] = sgp.TVOC;
    } else {
      new_data[2] = 0;
      new_data[3] = 0;
    }
     
    for(int i=0;i<dataAmount;i++) {
      if (isnan(new_data[i])) {
        new_data[i] = 0;
      }
      push(&data[i], new_data[i] / limits[i]);
    }
    display.clearDisplay();
    //If switch is on draw on display
    if (switchState) {
      checkWifiConnection();//Check if Wifi is still connected
      drawGraph(current_menu);
      drawHeader(current_menu);
      drawTime();
      drawSensorValues();
    }
    display.display();
  
    //Serial.println(ESP.getFreeHeap());
    /*Serial.print(dht.getStatusString());
    Serial.print("\t");
    Serial.print(new_data[0], 1);
    Serial.print("\t\t");
    Serial.print(new_data[1], 1);
    Serial.print("\t\t");
    Serial.print(dht.toFahrenheit(new_data[1]), 1);
    Serial.print("\t\t");
    Serial.print(dht.computeHeatIndex(new_data[1], new_data[0], false), 1);
    Serial.print("\t\t");
    Serial.println(dht.computeHeatIndex(dht.toFahrenheit(new_data[1]), new_data[0], true), 1);
    if (hasSGP) {
          Serial.print("CO2: ");
          Serial.print(new_data[2]);
          Serial.print(",");
          Serial.print(" ppm\tTVOC: ");
          Serial.println(new_data[3]); 
    }*/
  }

  //Logging to file
  if (hasSD && millis() >= t3 + 60000) {// Every 60 sec
    t3 = millis();
    logFile = SD.open(fileName, FILE_WRITE);
     
    if (!logFile) {
      Serial.println("Couldn't open file");
    } else {
    
      timeClient.update();
      epoch = timeClient.getEpochTime() - startEpoch;

      // Write Time
      logFile.print(timeClient.getFormattedTime());
      logFile.print(","); 

      //Write Sensor Data
      for(int i=0;i<dataAmount;i++) {
        if (isnan(new_data[i])) {
          new_data[i] = -1;
        }
        logFile.print(new_data[i]);
        if (i <= dataAmount-1) logFile.print(","); 
      }
  
      logFile.println();
      logFile.close(); 
    }
  }

  //Checking Switch
  bool reading = analogRead(switchPin) < 500 ? true: false;
  
  if (reading != lastSwitchState) {
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (reading != switchState) { // Switch Position Changed
      switchState = reading;
      Serial.print("Switch: ");
      Serial.println(switchState);
      if (!switchState) {
        // Change Menu
        current_menu = (current_menu + 1) % dataAmount;  
      }
    }
  }
  lastSwitchState = reading;

  if (ESP.getFreeHeap() <= 5000) {
    ESP.restart();
  }
}


void drawHeader(uint8_t idx) {
  for(int i=0;i<dataAmount;i++) {
    display.setCursor(i*20,0);
    for(int j=0;j<3;j++) {
      display.write(header[i][j]);
    }
    display.println();
    if (i==idx) {
      display.drawLine(i*20,11,i*20+15,11,WHITE);
    }
  }
}

void drawTime() {
  display.setCursor(80,0);
  display.println(timeClient.getFormattedTime());
}

void drawSensorValues() {
  for (uint8_t i=0;i<dataAmount;i++) {
    display.setCursor(0,15 + 10*i);
    display.print((int)new_data[i]);
    switch (i) {
      case 0: display.print(" %");
              break;
      case 1: display.print(" C");
              break;
    }
    
  }
}

bool checkWifiConnection() {
  if (WiFi.isConnected()) {
    return true;
  }
  Serial.println("No Wifi Connection");

  wifiMulti.run(1000);
  
  display.setCursor(0,15 + 10*3);
  display.println("No Wifi");

  return false;
}


void drawGraph(int idx) {
  display.fillRect(25,14,graphWidth,14+graphHeight, BLACK);
  display.drawRect(25,14,graphWidth,graphHeight, WHITE);
  for(int i=0;i<dataLen;i++) {
    display.drawPixel(25 + (float)i/dataLen * graphWidth, 14 + graphHeight- (get(&data[idx],i) * graphHeight), WHITE);
  }
}

void push(Fifo *fifo, float element) {
  fifo->head = (fifo->head + 1) % dataLen;
  fifo->data[fifo->head] = element;
}

float get(Fifo *fifo, int idx) {
  idx = (fifo->head + idx) % dataLen;
  if (idx < 0) {
    idx = dataLen + idx;
  }
  
  return fifo->data[abs(idx)];
}

uint32_t getAbsoluteHumidity(float temperature, float humidity) {
    // approximation formula from Sensirion SGP30 Driver Integration chapter 3.15
    const float absoluteHumidity = 216.7f * ((humidity / 100.0f) * 6.112f * exp((17.62f * temperature) / (243.12f + temperature)) / (273.15f + temperature)); // [g/m^3]
    const uint32_t absoluteHumidityScaled = static_cast<uint32_t>(1000.0f * absoluteHumidity); // [mg/m^3]
    return absoluteHumidityScaled;
}

// read wifi data from sd card and try to connect to networks
void connectWifi() {
   File creds = SD.open(credsFileName);
   char ssidBuffer[30];
   char pwBuffer[30];
   bool ssid = true;
   int idx = 0;
   while (creds.available()) {
    char c = creds.read();
    
    if (c != ',' && c != '\n') {
      if (ssid) {
        ssidBuffer[idx] = c;
      } else {
        pwBuffer[idx] = c;
      }
      idx ++;
    } else {
      if (c == ',' && ssid) {
        while (idx < 30) {
          ssidBuffer[idx] = '\0';
          idx++;
        }
        idx = 0;
        ssid = false;
        
      } else if (c == '\n') {
        while (idx < 30) {
          pwBuffer[idx] = '\0';
          idx++;
        }
        idx = 0;
        ssid = true; 

        Serial.print("Connecting to: ");
        Serial.print(ssidBuffer);
        Serial.print(" with pw: ");
        Serial.println(pwBuffer);
        
        wifiMulti.addAP(ssidBuffer, pwBuffer);
      }
    }
  }
  creds.close();

  while (wifiMulti.run(connectTimeoutMs) != WL_CONNECTED) {
    Serial.print(".");
    delay(20); 
  }
  Serial.println();
  Serial.println("Connected");
}
